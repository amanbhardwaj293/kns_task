const successResponse = (res, data = null, message = "Success") => {
  res.status(200).json({
    status: "success",
    data,
    message,
  });
};

const createdResponse = (res, data, message = "Created") => {
  res.status(201).json({
    status: "created",
    data,
    message,
  });
};

const updatedResponse = (res, data, message = "Updated") => {
  res.status(200).json({
    status: "success",
    data,
    message,
  });
};

const notFoundResponse = (res, message = "Not Found") => {
  res.status(404).json({
    status: "not found",
    message,
  });
};

const notRegistered = (res, message = "Email Not registered") => {
  res.status(404).json({
    status: "not Registred",
    message,
  });
};

const alreadyExist = (res, message = " Already Exist") => {
  res.status(400).json({
    status: "already Exist",
    message,
  });
};

const notCreated = (res, message = "not created") => {
  res.status(400).json({
    status: "not created",
    message,
  });
};

const unauthorizedResponse = (res, message = "Unauthorized") => {
  res.status(401).json({
    staus: "unauthorized",
    message,
  });
};

const responses = {
  notCreated,
  updatedResponse,
  notFoundResponse,
  successResponse,
  createdResponse,
  alreadyExist,
  unauthorizedResponse,
  notRegistered,
};

export default responses;
