import User from "../model/user.model.js";
import responses from "../utils/responses.js";
export const get = async (req, res) => {
  try {
    // if (!req.params.id)
    //   return responses.notFoundResponse(res, { message: "User ID required" });
    let user = await User.find();
    if (!user) return responses.notFoundResponse(res, "User Not Found..");

    let response = { user };
    return responses.successResponse(res, response, "User Found..");
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "ServerError Occured" });
  }
};
