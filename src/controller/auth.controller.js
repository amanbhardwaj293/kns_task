import User from "../model/user.model.js";
import responses from "../utils/responses.js";
import bcrypt from "bcrypt";
export const register = async (req, res) => {
  console.log("inside register");
  try {
    console.log("inside try", req.body);
    let user = await User.findOne({
      $or: [
        {
          mobile: req.body.mobile,
        },
        {
          email: req.body.email,
        },
      ],
    });
    if (user) {
      return responses.alreadyExist(res, { message: "Already exist" });
    }
    console.log("inside try1");
    const salt = await bcrypt.genSalt(10);
    console.log("inside try2");
    const hash_password = await bcrypt.hash(req.body.password, salt);
    console.log("inside try3");
    req.body.password = hash_password;

    let new_user = await User.create(req.body);
    console.log("inside try4");
    if (new_user) {
      return responses.successResponse(res, {
        message: "registered successfully",
      });
    } else {
      return responses.notCreated(res, { message: "not created" });
    }
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "ServerError Occured" });
  }
};

export const login = async (req, res) => {
  try {
    console.log("abcd", req.body);
    let user = await User.findOne({ email: req.body.email });
    if (!user) {
      return responses.notRegistered(res, { message: "not exist" });
    }
    let password = user.password;
    let matched = await bcrypt.compareSync(req.body.password, password);

    if (!matched) {
      return responses.unauthorizedResponse(res, {
        message: "Password not matched",
      });
    }
    return responses.successResponse(res, { message: "Login Successfully" });
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: "ServerError Occured" });
  }
};
