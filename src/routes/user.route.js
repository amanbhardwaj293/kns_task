import * as controllers from "../controller/user.controller.js";
import express from "express";
const router = express.Router();

router.get("/list", controllers.get);

export default router;
