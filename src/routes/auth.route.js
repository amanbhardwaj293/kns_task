import * as controllers from "../controller/auth.controller.js";
import express from "express";
const router = express.Router();

router.use(function (req, res, next) {
  res.header(
    "Access-Control-Allow-Headers",
    "x-access-token, Origin, Content-Type, Accept"
  );
  next();
});

router.post("/register", controllers.register);
router.post("/login", controllers.login);

export default router;
