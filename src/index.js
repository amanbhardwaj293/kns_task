import express from "express";
import bodyParser from "body-parser";
const app = express();
import mongoose from "mongoose";
import { default as AuthRoutes } from "../src/routes/auth.route.js";
import { default as UserRoutes } from "../src/routes/user.route.js";

mongoose
  .connect(`mongodb+srv://nikkumar334:Nikkumar2276@ans.y96vfkl.mongodb.net/`)
  .then(() => {
    console.log("Database Connected...");
  })
  .catch((error) => {
    console.log(error);
  });

app.get("/", (req, res) => {
  return res.send("welcome to KNS");
});

app.use(bodyParser.json());

app.use("/api/auth", AuthRoutes);
app.use("/api/user", UserRoutes);
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Port listening on ${PORT}`);
});
